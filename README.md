## Tailscale in GitLab CI/CD runners

This `.gitlab-ci.yml` file demonstrates how to join a
[Tailscale tailnet](https://www.tailscale.com) from a GitLab
CI/CD runner, in order to connect privately to resources like
databases or other services.

To use this functionality you need to create a repository
variable named TAILSCALE\_AUTHKEY holding an [authkey](https://tailscale.com/kb/1085/auth-keys/) 
for the Tailnet to be accessed. [Ephemeral authkeys](https://tailscale.com/kb/1111/ephemeral-nodes/)
tend to be a good fit for GitLab runners, as they clean up their state
automatically shortly after the runner finishes.

If you use Hashicorp Vault or other means of making secrets accessible
to GitLab Runners, the TAILSCALE\_AUTHKEY should preferentially be stored
as a secret.
